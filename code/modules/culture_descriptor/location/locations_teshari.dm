/decl/cultural_info/location/qerrbalak/teshari
	name = HOME_SYSTEM_TESHARI_SIRISAI
	description = "Sirisai is the homeworld of the Teshari. It is a colder planet, but still within the Goldilocks zone, as such, creatures and plants on the planet are adapted towards colder and darker climates."
	capital = "None"
	ruling_body = "None"
	distance = "71 light years"
	economic_power = 1.0
	secondary_langs = list(
		LANGUAGE_SPACER,
		LANGUAGE_SIGN
	)