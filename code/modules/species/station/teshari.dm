/datum/species/teshari
	name = SPECIES_TESHARI
	name_plural = "Teshari"
	description = "A race of feathered raptors who developed on a cold world, near \
	the far edge of the Goldilocks zone. Extremely fragile, they\
	developed hunting skills that emphasized taking out \
	their prey without themselves getting hit. They are \
	a pack based species, living in large groups together."
	min_age = 15
	max_age = 45
	hidden_from_codex = FALSE
	health_hud_intensity = 3
	blood_color = "#d514f7"
	flesh_color = "#5f7bb0"
	base_color = "#001144"
	tail = "teshari_tail"
	tail_hair = "feathers"
	strength = STR_HIGH
	reagent_tag = IS_TESHARI
	breath_pressure = 12

	default_h_style = "Teshari Ears"
	default_f_style = "Shaved"

	move_trail = /obj/effect/decal/cleanable/blood/tracks/paw

	icobase = 			'icons/mob/human_races/species/teshari/body.dmi'
	deform = 			'icons/mob/human_races/species/teshari/deformed_body.dmi'
	damage_overlays = 	'icons/mob/human_races/species/teshari/damage_overlay.dmi'
	damage_mask = 		'icons/mob/human_races/species/teshari/damage_mask.dmi'
	blood_mask = 		'icons/mob/human_races/species/teshari/blood_mask.dmi'
	preview_icon =		'icons/mob/human_races/species/teshari/preview.dmi'
	husk_icon = 		'icons/mob/human_races/species/teshari/husk.dmi'

	limb_blend		= ICON_MULTIPLY
	tail_blend		= ICON_MULTIPLY

	slowdown = -0.8 //speed fix?

	darksight_tint = DARKTINT_GOOD
	flash_mod = 2
	total_health = 150
	brute_mod = 1.35
	burn_mod =  1.35
	metabolism_mod = 2.0
	mob_size = MOB_SMALL
	holder_type = /obj/item/weapon/holder/human
	light_sensitive = 6
	gluttonous = GLUT_TINY
	blood_volume = 280
	hunger_factor = DEFAULT_HUNGER_FACTOR * 3
	taste_sensitivity = TASTE_SENSITIVE
	pulse_rate_mod = 1.5
	body_temperature = 314.15
	spawn_flags = SPECIES_CAN_JOIN | SPECIES_IS_WHITELISTED
	appearance_flags = HAS_HAIR_COLOR | HAS_SKIN_COLOR | HAS_EYE_COLOR
	bump_flag = MONKEY
	swap_flags = MONKEY|SLIME|SIMPLE_ANIMAL
	push_flags = MONKEY|SLIME|SIMPLE_ANIMAL|ALIEN
	cold_level_1 = 180
	cold_level_2 = 130
	cold_level_3 = 70
	heat_level_1 = 320
	heat_level_2 = 370
	heat_level_3 = 600
	heat_discomfort_level = 292
	heat_discomfort_strings = list(
		"Your feathers prickle in the heat.",
		"You feel uncomfortably warm.",
		"Your hands and feet feel hot as your body tries to regulate heat")
	cold_discomfort_level = 200
	cold_discomfort_strings = list(
		"You feel a bit chilly.",
		"You fluff up your feathers against the cold.",
		"You move your arms closer to your body to shield yourself from the cold.",
		"You press your ears against your head to conserve heat.",
		"Your feathers bristle against the cold.")
	ambiguous_genders = TRUE //Tesh genders are hard to tell for non-tesh
	standing_jump_range = 4 //teshari are agile little raptors

	has_limbs = list(
		BP_CHEST =  list("path" = /obj/item/organ/external/chest),
		BP_GROIN =  list("path" = /obj/item/organ/external/groin),
		BP_HEAD =   list("path" = /obj/item/organ/external/head),
		BP_L_ARM =  list("path" = /obj/item/organ/external/arm),
		BP_R_ARM =  list("path" = /obj/item/organ/external/arm/right),
		BP_L_LEG =  list("path" = /obj/item/organ/external/leg),
		BP_R_LEG =  list("path" = /obj/item/organ/external/leg/right),
		BP_L_HAND = list("path" = /obj/item/organ/external/hand/teshari),
		BP_R_HAND = list("path" = /obj/item/organ/external/hand/right/teshari),
		BP_L_FOOT = list("path" = /obj/item/organ/external/foot/teshari),
		BP_R_FOOT = list("path" = /obj/item/organ/external/foot/right/teshari)
		)
	has_organ = list(
		BP_HEART =    /obj/item/organ/internal/heart,
		BP_LUNGS =    /obj/item/organ/internal/lungs,
		BP_LIVER =    /obj/item/organ/internal/liver/teshari,
		BP_STOMACH =  /obj/item/organ/internal/stomach,
		BP_KIDNEYS =  /obj/item/organ/internal/kidneys/teshari,
		BP_BRAIN =    /obj/item/organ/internal/brain,
		BP_EYES =     /obj/item/organ/internal/eyes/teshari
		)

	unarmed_types = list(
		/datum/unarmed_attack/claws,
		/datum/unarmed_attack/bite/sharp,
		/datum/unarmed_attack/punch,
		/datum/unarmed_attack/stomp/weak
		)

	inherent_verbs = list(
		/mob/living/carbon/human/proc/sonar_ping,
		/mob/living/proc/toggle_pass_table
		)

	descriptors = list(
		/datum/mob_descriptor/height = -8,
		/datum/mob_descriptor/build = -8
		)

	available_cultural_info = list(
		TAG_CULTURE = list(
			CULTURE_TESHARI,
			CULTURE_OTHER
		),
		TAG_HOMEWORLD = list(
			HOME_SYSTEM_TESHARI_SIRISAI,
			HOME_SYSTEM_OTHER
		),
		TAG_FACTION = list(
			FACTION_NANOTRASEN,
			FACTION_FREETRADE,
			FACTION_HEPHAESTUS,
			FACTION_XYNERGY,
			FACTION_EXPEDITIONARY,
			FACTION_CORPORATE,
			FACTION_DAIS,
			FACTION_TESHARI,
			FACTION_OTHER
		),
		TAG_RELIGION =  list(
			RELIGION_AGNOSTICISM,
			RELIGION_ATHEISM,
			RELIGION_TESHARI_SPIRITS,
			RELIGION_OTHER
		)
	)

/datum/species/teshari/get_surgery_overlay_icon(var/mob/living/carbon/human/H)
	return 'icons/mob/human_races/species/teshari/surgery.dmi'

/datum/species/teshari/skills_from_age(age)
	switch(age)
		if(0 to 17)		. = -4
		if(18 to 25)	. = 0
		if(26 to 35)	. = 4
		else			. = 8